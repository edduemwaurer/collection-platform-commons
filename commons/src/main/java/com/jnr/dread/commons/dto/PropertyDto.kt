package com.jnr.dread.commons.dto

data class PropertyDto(val name: String)