package com.jnr.dread.commons.dbmodels

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.jnr.dread.commons.domain.DomainResourceActions
import com.jnr.dread.commons.domain.DomainResources

@Entity(tableName = "ResourceAction")
data class DatabaseResourceActions(
    @PrimaryKey(autoGenerate = true) val id: Long,
    val resourceId: Long,
    val name: String,
    val link: String,
    val icon: String
)

fun List<DatabaseResourceActions>.asDomainResourcesActions(): List<DomainResourceActions> {
    return map {
        DomainResourceActions(
            _resource = it.resourceId,
            _resourceActionId = it.id,
            _resourceActionName = it.name,
            _resourceActionLink = it.link,
            _resourceActionIcon = it.icon
        )
    }
}