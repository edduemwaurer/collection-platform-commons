package com.jnr.dread.commons.domain

data class DomainAppFeatures(val _id: Int, val _name: String, val _icon: String, val _link: String)
