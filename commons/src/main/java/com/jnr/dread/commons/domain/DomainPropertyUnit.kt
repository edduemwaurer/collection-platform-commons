package com.jnr.dread.commons.domain

data class DomainPropertyUnit(
    val id: Long,
    val name: String,
    val propertyId: Int,
    val code: String,
    val category: String,
    val cost: Double,
    var isAllocated: Boolean,
    var categoryImage: Int?,
    val tenant: String,
    var paymentStatusIcon: String,
    val rentIsPayed: Boolean,
    var showNudgeButton: Boolean?
)