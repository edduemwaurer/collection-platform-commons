package com.jnr.dread.commons.config

interface ListViewItemClickListener<T> {
    fun onItemClick(item: T)
}