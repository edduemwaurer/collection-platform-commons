package com.jnr.dread.commons.domain

import com.jnr.dread.commons.BuildConfig

data class DomainAppDescription(
    val appName: String = BuildConfig.VERSION_NAME,
    val appIcon: Int = 1,
    val appShortDesc: String = ""
)