package com.jnr.dread.commons.domain

data class DomainResourceActions(
    val _resourceActionId: Long,
    val _resource: Long,
    val _resourceActionName: String,
    val _resourceActionIcon: String,
    val _resourceActionLink: String
)