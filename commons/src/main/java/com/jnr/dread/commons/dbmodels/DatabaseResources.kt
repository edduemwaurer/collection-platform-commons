package com.jnr.dread.commons.dbmodels

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.jnr.dread.commons.domain.DomainResources

@Entity(tableName = "Resource")
data class DatabaseResources(
    @PrimaryKey(autoGenerate = true) val id: Long,
    val name: String,
    val link: String,
    val icon: String
)


fun List<DatabaseResources>.asDomainResources(): List<DomainResources> {
    return map {
        DomainResources(
            _resourceId = it.id,
            _resourceName = it.name,
            _resourceLink = it.link,
            _resourceIcon = it.icon
        )
    }
}