package com.jnr.dread.commons.domain

data class DomainResources(
    val _resourceId: Long,
    val _resourceName: String,
    val _resourceIcon: String,
    val _resourceLink: String
)