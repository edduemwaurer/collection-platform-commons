package com.jnr.dread.commons.domain

data class DomainProperty(
    val id: Long,
    val name: String,
    val description: String,
    val location: String,
    val rating: String,
    val imageUrl: String,
    val totalUnits: String,
    val propertyCategory: String,
    val images: List<String>,
    val expectedAmount: String,
    val collectedAmount: String,
    val allocatedUnits: String,
    val outstandingBalance: String,
    val vacantUnits: String
)