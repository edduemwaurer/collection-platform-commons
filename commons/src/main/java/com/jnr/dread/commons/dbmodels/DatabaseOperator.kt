package com.jnr.dread.commons.dbmodels

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.jnr.dread.commons.domain.DomainOperator

@Entity(tableName = "Operator")
data class DatabaseOperator(
    @PrimaryKey(autoGenerate = true) val id: Long,
    val name: String
)

fun List<DatabaseOperator>.asDomainOperator(): List<DomainOperator> {
    return map {
        DomainOperator(
            name = it.name
        )
    }
}