package com.jnr.dread.commons.domain

data class DomainOperator(val name: String)