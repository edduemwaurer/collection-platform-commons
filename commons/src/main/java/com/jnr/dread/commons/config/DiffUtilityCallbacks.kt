package com.jnr.dread.commons.config

import com.jnr.dread.commons.domain.DomainPropertyUnit

val domainPropertyUnitDiffCallback = object : QueryItemDiffCallback<DomainPropertyUnit>() {}
