package com.jnr.dread.commons.dto

data class OperatorDTO(val name: String)