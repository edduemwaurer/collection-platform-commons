package com.jnr.dread.commons.config

interface QueryItem<T> {
    val item: T
    val id: String
}