package com.jnr.dread.commons.dbmodels

import com.jnr.dread.commons.domain.DomainPropertyUnit

data class DatabasePropertyUnit(
    val id: Long,
    val name: String,
    val propertyId: String,
    val code: String,
    val category: String,
    val cost: String,
    val isAllocated: Boolean,
    var categoryImage: String,
    val tenant: String,
    var paymentStatusIcon: String,
    val rentIsPayed: Boolean,
    var showNudgeButton: Boolean
)

fun List<DatabasePropertyUnit>.asDomainPropertyUnit(): List<DomainPropertyUnit> {
    return map {
        DomainPropertyUnit(
            id = it.id,
            name = it.name,
            propertyId = it.propertyId.toInt(),
            code = it.code,
            category = it.category,
            cost = it.cost.toDouble(),
            isAllocated = true,
            categoryImage = 1,
            tenant = it.tenant,
            paymentStatusIcon = it.paymentStatusIcon,
            rentIsPayed = it.rentIsPayed,
            showNudgeButton = it.showNudgeButton
        )
    }
}