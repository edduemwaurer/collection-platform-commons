package com.jnr.dread.commons.dbmodels

import com.jnr.dread.commons.domain.DomainProperty

data class DatabaseProperty(
    val id: Long,
    val name: String,
    val propertyId: String,
    val code: String,
    val category: String,
    val cost: String,
    val isAllocated: Boolean,
    var categoryImage: String,
    val tenant: String,
    var paymentStatusIcon: String,
    val rentIsPayed: Boolean,
    var showNudgeButton: Boolean
)

/*
fun List<DatabaseProperty>.asDomainProperty(): List<DomainProperty> {
    return map {
        DomainProperty(
            id = it.id,
            name = it.name,
            propertyId = it.propertyId.toInt(),
            code = it.code,
            category = it.category,
            cost = it.cost.toDouble(),
            isAllocated = true,
            categoryImage = 1,
            tenant = it.tenant,
            paymentStatusIcon = it.paymentStatusIcon,
            rentIsPayed = it.rentIsPayed,
            showNudgeButton = it.showNudgeButton
        )
    }
}*/
